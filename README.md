# SmartMonitoring

Before you can build this project, you must install and configure the following dependencies on your machine:

Node.js.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    npm install

Run the following commands in two separate terminals to start the application

    ./mvnw
    npm start
