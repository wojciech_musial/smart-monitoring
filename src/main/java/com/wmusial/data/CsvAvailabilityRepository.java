package com.wmusial.data;

import com.wmusial.domain.Company;
import com.wmusial.domain.SystemAvailability;
import com.wmusial.domain.SystemAvailabilityRepository;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CsvAvailabilityRepository implements SystemAvailabilityRepository {

    @Override
    public List<SystemAvailability> getCurrentAvailability() {
        List<SystemAvailability> availabilities = read(1);
        return availabilities.stream()
            .sorted((o1, o2) -> o2.getDate().compareTo(o1.getDate()))
            .collect(Collectors.groupingBy(SystemAvailability::getDate))
            .entrySet()
            .stream()
            .findFirst()
            .map(Map.Entry::getValue)
            .orElse(Collections.emptyList());
    }

    @Override
    public List<SystemAvailability> getAvailability(int pastDays) {
        return read(pastDays);
    }

    @SneakyThrows
    private synchronized List<SystemAvailability> read(int pastDays) {

        List<SystemAvailability> availabilities;
        InputStream resource = new ClassPathResource(
            "SYSTEMAVAILABILITY.csv").getInputStream();
        try ( BufferedReader reader = new BufferedReader(
            new InputStreamReader(resource)) ) {
            availabilities = reader.lines()
                .skip(1)
                .map(line -> line.split(";"))
                .map(row -> SystemAvailability.builder()
                .id(Long.valueOf(row[0]))
                .date(parseDate(row[2]))
                .company(Company.builder()
                    .id(Long.valueOf(row[3]))
                    .name(row[4])
                    .build())
                .location(row[6])
                .availabilityIndex(Integer.parseInt(row[7]))
                .roomsQuantity(Integer.parseInt(row[8]))
                .roomIssuesQuantity(Integer.parseInt(row[9]))
                .lastMonthAvailabilityIndex(Integer.parseInt(row[10]))
                .build())
                .sorted((o1, o2) -> o2.getDate().compareTo(o1.getDate()))
                .collect(Collectors.groupingBy(SystemAvailability::getDate))
            .entrySet()
            .stream()
            .limit(pastDays)
            .flatMap(e -> e.getValue().stream())
            .collect(Collectors.toList());
        }

        return availabilities;
    }

    private LocalDate parseDate(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
    }
}
