package com.wmusial;

import com.wmusial.data.CsvAvailabilityRepository;
import com.wmusial.domain.SystemAvailabilityRepository;
import com.wmusial.domain.SystemAvailabilityService;
import com.wmusial.usecase.SystemAvailabilityUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {

    @Bean
    public SystemAvailabilityRepository systemAvailabilityRepository() {
        return new CsvAvailabilityRepository();
    }

    @Bean
    public SystemAvailabilityUseCase systemAvailabilityUseCase(SystemAvailabilityService systemAvailabilityService) {
        return new SystemAvailabilityUseCase(systemAvailabilityService);
    }
}
