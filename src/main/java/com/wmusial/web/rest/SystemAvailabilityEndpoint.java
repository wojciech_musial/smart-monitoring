package com.wmusial.web.rest;

import com.wmusial.usecase.SystemAvailabilityUseCase;
import com.wmusial.web.rest.dto.HistoricalAvailabilityDto;
import com.wmusial.web.rest.dto.TodaysAvailabilityDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class SystemAvailabilityEndpoint {

    private final SystemAvailabilityUseCase systemAvailabilityUseCase;

    @GetMapping("/api/availability/today")
    public TodaysAvailabilityDto getTodaysAvailability() {
        return systemAvailabilityUseCase.getTodaysAvailability();
    }

    @GetMapping("/api/availability")
    public HistoricalAvailabilityDto getHistoricalAvailability(@RequestParam(required = false, defaultValue = "6") int pastDays) {
        return systemAvailabilityUseCase.getHistoricalAvailability(pastDays);
    }
}
