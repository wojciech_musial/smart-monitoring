/**
 * View Models used by Spring MVC REST controllers.
 */
package com.wmusial.web.rest.vm;
