package com.wmusial.web.rest.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class TodaysAvailabilityDto {

    private int todaysAvailabilityIndex;
    private int todaysAvailabilityIndexLastMonthDelta;

    private List<LocationAvailabilityDto> locationData;
}
