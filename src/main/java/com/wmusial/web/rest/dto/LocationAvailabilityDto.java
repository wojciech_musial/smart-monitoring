package com.wmusial.web.rest.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Value;


@Builder
public class LocationAvailabilityDto {
    String location;
    int availabilityIndex;
    int availabilityIndexLastMonthDelta;
    int roomIssuesQuantity;
    int roomQuantity;

    public String getLocation() {
        return location;
    }

    public int getAvailabilityIndex() {
        return availabilityIndex;
    }

    public int getAvailabilityIndexLastMonthDelta() {
        return availabilityIndexLastMonthDelta;
    }

    public int getRoomIssuesQuantity() {
        return roomIssuesQuantity;
    }

    public int getRoomQuantity() {
        return roomQuantity;
    }
}
