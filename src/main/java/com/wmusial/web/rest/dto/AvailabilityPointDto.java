package com.wmusial.web.rest.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AvailabilityPointDto {
    private int availabilityIndex;
    private String label;
}
