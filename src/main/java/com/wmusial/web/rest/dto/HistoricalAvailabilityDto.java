package com.wmusial.web.rest.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class HistoricalAvailabilityDto {
    private final List<AvailabilityPointDto> points;
}
