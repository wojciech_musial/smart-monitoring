package com.wmusial.usecase;

import com.wmusial.domain.SystemAvailability;
import com.wmusial.domain.SystemAvailabilityService;
import com.wmusial.web.rest.dto.AvailabilityPointDto;
import com.wmusial.web.rest.dto.HistoricalAvailabilityDto;
import com.wmusial.web.rest.dto.LocationAvailabilityDto;
import com.wmusial.web.rest.dto.TodaysAvailabilityDto;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class SystemAvailabilityUseCase {

    private final SystemAvailabilityService systemAvailabilityService;

    public TodaysAvailabilityDto getTodaysAvailability() {
        List<SystemAvailability> availabilities = systemAvailabilityService.getTodaysAvailability();
        LocationAvailabilityDto average = calculateAverage(availabilities);
        List<LocationAvailabilityDto> locationData = availabilities.stream()
            .map(this::toDto)
            .collect(Collectors.toList());

        return TodaysAvailabilityDto.builder()
            .todaysAvailabilityIndex(average.getAvailabilityIndex())
            .todaysAvailabilityIndexLastMonthDelta(average.getAvailabilityIndexLastMonthDelta())
            .locationData(locationData)
            .build();
    }

    public HistoricalAvailabilityDto getHistoricalAvailability(int pastDays) {
        List<SystemAvailability> historicalAvailability = systemAvailabilityService.getHistoricalAvailability(pastDays);
        List<AvailabilityPointDto> points = toPointsDto(historicalAvailability);

        return HistoricalAvailabilityDto.builder()
            .points(points)
            .build();
    }

    private List<AvailabilityPointDto> toPointsDto(List<SystemAvailability> historicalAvailability) {
        return historicalAvailability.stream()
            .collect(Collectors.groupingBy(SystemAvailability::getDay, Collectors.averagingInt(SystemAvailability::getAvailabilityIndex)))
            .entrySet()
            .stream()
            .map(entry -> AvailabilityPointDto.builder().label(entry.getKey()).availabilityIndex(entry.getValue().intValue()).build())
            .sorted(Comparator.comparing(AvailabilityPointDto::getLabel))
            .collect(Collectors.toList());
    }

    private LocationAvailabilityDto toDto(SystemAvailability availability) {
        return LocationAvailabilityDto.builder()
            .location(availability.getLocation())
            .availabilityIndex(availability.getAvailabilityIndex())
            .availabilityIndexLastMonthDelta(availability.getLastMonthDelta())
            .roomQuantity(availability.getRoomsQuantity())
            .roomIssuesQuantity(availability.getRoomIssuesQuantity())
            .build();
    }

    private LocationAvailabilityDto calculateAverage(List<SystemAvailability> availabilities) {
        double averageAvailability = availabilities.stream()
            .mapToInt(SystemAvailability::getAvailabilityIndex)
            .average()
            .orElseThrow(() -> new IllegalStateException("System availability cannot be empty"));

        double averageDelta = availabilities.stream()
            .mapToInt(SystemAvailability::getLastMonthDelta)
            .average()
            .orElseThrow(() -> new IllegalStateException("System availability cannot be empty"));

        return LocationAvailabilityDto.builder()
            .availabilityIndex((int) averageAvailability)
            .availabilityIndexLastMonthDelta((int) averageDelta)
            .build();
    }

}
