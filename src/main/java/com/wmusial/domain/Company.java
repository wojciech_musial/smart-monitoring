package com.wmusial.domain;

import lombok.Builder;
import lombok.Value;

@Builder
public class Company {
    private Long id;
    private String name;
}
