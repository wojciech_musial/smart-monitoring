package com.wmusial.domain;

import java.util.List;

public interface SystemAvailabilityRepository {
    List<SystemAvailability> getCurrentAvailability();
    List<SystemAvailability> getAvailability(int pastDays);
}
