package com.wmusial.domain;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

@Value
@Builder
public class SystemAvailability {
    Long id;
    LocalDate date;
    Company company;
    String location;
    int availabilityIndex;
    int roomsQuantity;
    int roomIssuesQuantity;
    int lastMonthAvailabilityIndex;

    public int getLastMonthDelta() {
        return availabilityIndex - lastMonthAvailabilityIndex;
    }

    public String getDay() {
        return String.format("%s-%s",
            date.getMonth().getDisplayName(TextStyle.FULL, Locale.US),
            date.getDayOfMonth());
    }
}
