package com.wmusial.domain;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SystemAvailabilityService {

    private final SystemAvailabilityRepository repository;

    public List<SystemAvailability> getTodaysAvailability() {
        return repository.getCurrentAvailability();
    }

    public List<SystemAvailability> getHistoricalAvailability(int pastDays) {
        return repository.getAvailability(pastDays);
    }
}
