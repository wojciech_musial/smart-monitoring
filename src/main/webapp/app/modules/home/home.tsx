import './home.scss';

import React from 'react';
import axios from 'axios';

import {connect} from 'react-redux';
import {Col, Row} from 'reactstrap';
import {HistoricalAvailability} from "app/modules/home/HistoricalAvailability";
import {SystemAvailability} from "app/modules/home/SystemAvailability";
import {LoginPrompt} from "app/modules/home/LoginPrompt";



export class Home extends React.Component<{ account: any }, { availability: any, historical: any }> {
  constructor(props) {
    super(props);
    this.state = {
      availability: {
        todaysAvailabilityIndex: 0,
        todaysAvailabilityIndexLastMonthDelta: 0,
        locationData: []
      },
      historical: {
        points: []
      }
    };
    this.getTodaysAvailability();
    this.getHistoricalAvailability();
  }

  getTodaysAvailability = () => {
    axios.get('api/availability/today')
      .then(resp => this.setState({
        availability: resp.data,
        historical: this.state.historical
      }));
  };

  getHistoricalAvailability = () => {
    axios.get('api/availability')
      .then(resp => {
        this.setState({
          availability: this.state.availability,
          historical: resp.data
        })
      });
  };


  render() {
    return (
      <div className="container">
        <LoginPrompt account={this.props.account}/>
        {this.props.account && this.props.account.login && (
          <Row>
            <Col>
              <SystemAvailability data={this.state.availability}/>
            </Col>
            <Col className="historicalAvailability">
              <HistoricalAvailability data={this.state.historical}/>
            </Col>
          </Row>
        )}
      </div>
    );
  }
}


const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

type StateProps = ReturnType<typeof mapStateToProps>;
//
export default connect(mapStateToProps)(Home);
