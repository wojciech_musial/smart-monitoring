import './home.scss';
import React from "react";
import {Doughnut} from "react-chartjs-2";
import {ChartOptions} from "chart.js";


const options: ChartOptions = {
  rotation: Math.PI,
  circumference: Math.PI
};

export const DonutChart = (props) => {

  const {mainChart, index} = props;

  const mainChartColor = mainChart ? '#4ca1ff' :
    index < 80 ? '#ff5b00' : index < 90 ? '#fffa58' : '#20ff65';

  const chartData = {
    labels: [],
    datasets: [
      {
        data: [index, 100 - index],
        backgroundColor: [mainChartColor, '#96a392']
      }
    ]
  };

  return (
    <div>
      <Doughnut data={chartData} options={options}/>
      <div className={mainChart ? 'mainChartLabel' : "chartLabel"}>{index}%</div>
      {mainChart && <div className="mainChartSubLabel">Availability</div>}
    </div>
  );
};
