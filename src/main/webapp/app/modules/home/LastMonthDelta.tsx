import './home.scss';
import React from "react";
import "chartjs-plugin-trendline";
import {Col, Row} from 'reactstrap';


export const LastMonthDelta = (props) => {

  const {mainChart, delta, rooms, issues} = props;
  const arrowClass = delta > 0 ? 'arrow-up' : 'arrow-down';

  return (
    <div className="lastMonthDelta">
      <Row>
        <Col>

          <div className={arrowClass}/>
          {Math.abs(delta)}% {mainChart ? 'Last Month' : ''}
        </Col>
      </Row>
      {!mainChart &&
      <Row>
        <Col>{issues} issues in {rooms} rooms</Col>
      </Row>
      }
    </div>
  );
};
