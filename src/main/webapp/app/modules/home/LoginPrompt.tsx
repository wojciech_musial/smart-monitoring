import {Link} from "react-router-dom";
import React from "react";
import {Col, Row, Alert} from 'reactstrap';

export const LoginPrompt = (props) => {
  const {account} = props;
  return (
    <Row>
      <Col md="9">
        <h2>Welcome!</h2>
        <p className="lead">This is homepage</p>
        {account && account.login ? (
          <div>
            <Alert color="success">You are logged in as user {account.login}.</Alert>
          </div>
        ) : (
          <div>
            <Alert color="warning">
              If you want to
              <Link to="/login" className="alert-link">
                {' '}
                sign in
              </Link>
              , you can try the default:
              <br/> Administrator (login=&quot;admin&quot; and password=&quot;admin&quot;)
            </Alert>
          </div>
        )}
      </Col>
    </Row>
  )
};


