import './home.scss';
import React from "react";

import {Col, Row} from 'reactstrap';
import {ChartOptions} from "chart.js";
import {LastMonthDelta} from "app/modules/home/LastMonthDelta";
import {DonutChart} from "app/modules/home/DonutChart";


export const SystemAvailability = (props) => {

  const {
    todaysAvailabilityIndex,
    todaysAvailabilityIndexLastMonthDelta,
    locationData} = props.data;

  return (
    <div className="systemAvailability container">

      <h3 className="systemAvailabilityTitle">System Availability</h3>

      <Row>
        <Col className="chartTitle">
          Today
        </Col>
      </Row>
      <Row>
        <Col md="2"/>
        <Col xs="12" md="7">
          <DonutChart mainChart={true} index={todaysAvailabilityIndex}/>
        </Col>
        <Col xs="12" md="3" className="mainChartDelta">
          <LastMonthDelta mainChart={true} delta={todaysAvailabilityIndexLastMonthDelta}/>
        </Col>
      </Row>

      {locationData && locationData.map(location => (
        <Row key={location.location} className="align-items-center locationAvailability">
          <Col xs="12" md="3" className="locationName">{location.location.toLowerCase()}</Col>
          <Col xs="12" md="5"><DonutChart index={location.availabilityIndex} mainChart={false}/></Col>
          <Col xs="12" md="4" className="lastMonthDeltaColumn">
            <LastMonthDelta
              delta={location.availabilityIndexLastMonthDelta}
              rooms={location.roomQuantity}
              issues={location.roomIssuesQuantity}
              mainChart={false}/>
          </Col>
        </Row>
      ))}
    </div>
  );
};
