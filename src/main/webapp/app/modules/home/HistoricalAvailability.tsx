import './home.scss';
import React from "react";

import {Col, Row} from 'reactstrap';
import {LineChart} from "app/modules/home/LineChart";


export const HistoricalAvailability = (props) => {

  const {points} = props.data;

  return (
    <div className="systemAvailability">

      <h3 className="systemAvailabilityTitle">System Availability</h3>

      <Row>
        <Col className="chartTitle">
          Last 6 days
        </Col>
      </Row>
      <Row>
        <Col>
          <LineChart points={points}/>
        </Col>
      </Row>
    </div>
  );
};
