import './home.scss';
import React from "react";
import {ChartData, Line} from "react-chartjs-2";
import "chartjs-plugin-trendline";

const getChartData = (points, labels) => {
  console.log(points);
  console.log(labels);

  if (points.length != 0) {

  }

  let chartData = {
    datasets: [
      {
        label: 'Average availability',
        data: points,
        fill: false,
        lineTension: 0
      }
    ],
    labels: labels
  };

  if (points.length > 0) {
    chartData.datasets[0]["trendlineLinear"] = {
      style: "#3e95cd",
      lineStyle: "dotted",
      width: 2
    }
  }

  return chartData;
};

export const LineChart = (props) => {

  const {points} = props;

  const data = getChartData(
    points.map(p => p.availabilityIndex),
    points.map(p => p.label)
  );

  return (
    <div>
      <Line data={data}/>
    </div>
  );
};
